//script for magnific popup
$(document).ready(function(){
    $("#gallery").magnificPopup({
        delegate: 'a',
        type: 'image',
        gallery: {
            enabled: true
        },
        zoom: {
            enabled: true,
            duration: 300,
            easing: 'ease-in-out',
            opener: function(openerElement){
                return openerElement.is('img') ? openerElement :
                openerElement.find('img');
            }
        }
    });
});
$(document).ready(function(){
    $("#team-members").owlCarousel({
        items:3,
        autoplaytrue: true,
        margin: 10,
        loop: true,
        smartSpeed: 600,
        autoplayHoverPause: true,
        dots: true,
    });
});
$(document).ready(function(){
    $("#members").owlCarousel({
        items:1,
        autoplaytrue: true,
        margin: 20,
        loop: true,
        nav: false,
        smartSpeed: 700,
        autoplayHoverPause: true,
        dots: true,
    });
});
$(document).ready(function(){
    $("#client-list").owlCarousel({
        items:5,
        autoplaytrue: true,
        margin: 20,
        loop: true,
        nav: false,
        smartSpeed: 700,
        autoplayHoverPause: true,
        dots: true,
    });
});
$(function(){
    $(window).scroll(function(){
        if($(window).scrollTop()>0){
            console.log("inside scroll");
            $("#navbar").css("background","rgba(0, 0, 0, 0.7)");
       }
       else{
           $("#navbar").css("background","transparent");
       }
    });
    $("#scrollUp").click(function(){
        let values=$(window).scrollTop();
        var intervals = setInterval(function(){
            if($(window).scrollTop()>0){
                values=values-1;
                $(window).scrollTop(values);
                if($(window).scrollTop()<=0){
                    console.log("cleared");
                    clearInterval(intervals);
                }
            }
        },1);
    });
});

$(function () {
    $('.counter').counterUp({
        delay: 10,
        time: 1000
    });
});
