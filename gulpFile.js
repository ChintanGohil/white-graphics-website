var gulp=require("gulp");
var watch=require("gulp-watch");
var postcss=require("gulp-postcss");
var autoprefixer=require("autoprefixer");
var cssvars=require("postcss-simple-vars");
var imports=require("postcss-import");
var nesteds=require("postcss-nested");
var mixins=require("postcss-mixins");
var browsersync=require("browser-sync").create();


gulp.task('css',function(){
    console.log("doig css task");
    return gulp.src("./app/assets/styles/style.css").pipe(postcss([imports,mixins,nesteds,cssvars,autoprefixer])).pipe(gulp.dest("./app/css"));
});

gulp.task('cssInject',function(){
    return gulp.src("./app/assets/styles/style.css").pipe(browsersync.stream());
});

gulp.task('watch',function(){
    browsersync.init({
        notify:false,
        server:{
            baseDir:"app"
        }
    });
    watch("./app/index.html",function(){
        console.log("reloading html");
        browsersync.reload();
    }); watch("./app/assets/styles/**/*.css",gulp.series("css","cssInject"));
});

























